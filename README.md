# Install #

```
pip3 install parrot

# SingleXpathSelector, MultiXpathSelector require lxml
pip3 install lxml==3.4.4

# SeleniumLoader require selenium
pip3 install selenium==2.48.0

```

# Example #

```python
from parrot.loader.loaders import UrllibLoader
from parrot.parser import SimpleParser
from parrot.selector.selectors import SingleXpathSelector, MultiXpathSelector
from parrot.formatter.base import SingleFormatter, MultiFormatter
from parrot.formatter.formatters import html_node_text_content, html_href


handlers = {
    'title': (
        SingleXpathSelector('//title'),
        SingleFormatter(html_node_text_content)
    ),
    'a': (
        MultiXpathSelector('//a'),
        MultiFormatter(html_href)
    )
}
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'
}
loader = UrllibLoader(headers)
parser = SimpleParser(handlers, loader)


if __name__ == '__main__':
    url = 'https://docs.python.org/3.4/whatsnew/3.4.html'
    print(parser.from_source(url))
```